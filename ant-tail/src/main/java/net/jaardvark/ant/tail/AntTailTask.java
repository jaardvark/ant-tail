package net.jaardvark.ant.tail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.regex.Pattern;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class AntTailTask extends Task implements TailerListener {

	/**
	 * Filename of the file to tail
	 */
	protected String file = null;
	/**
	 * Timeout after which to stop
	 */
	protected Long timeout = null;
	
	protected boolean failOnTimeout = false;
	
	protected boolean failOnRegex = false;
	
	protected boolean failOnMissingFile = true;

	protected Tailer tailer;
	
	protected PrintStream output = System.out;
	
	protected String regex = null;
	
	protected Pattern pattern = null;
	
	protected Thread tailThread;
	
	protected long regExFound = -1;
	
	protected long lineNo = 0;
	
	protected Throwable error = null;
	
	@Override
	public void execute() throws BuildException {
		if (regex!=null)
			pattern = Pattern.compile(regex);
		tailer = new Tailer(new File(file), this, 100, false, false, 4096);
		tailThread = new Thread(tailer, "AntTailTask");
		tailThread.setDaemon(true);
		tailThread.start();
		// now wait for the tailer to finish
		long msTimeout = 0;
		if (timeout!=null)
			msTimeout = timeout * 1000;
		try {
			tailThread.join(msTimeout);
		} catch (InterruptedException e) {
			error = e;
			output.append("Tail was terminated due to interruption.\n");
		}
		if (error!=null){
			if (error instanceof FileNotFoundException && !failOnMissingFile)
				output.append("Ignoring FileNotFoundException...");
			else
				throw new BuildException("Error caused termination of tail.");
		}
		else if (regExFound>=0){
			if (failOnRegex)
				throw new BuildException("Pattern '"+regex+"' was found in line "+regExFound+" of the output.");
			else
				output.append("Pattern '"+regex+"' was found in line "+regExFound+" of the output, terminating tail.\n");
		}
		else if (timeout!=null){
			if (failOnTimeout)
				throw new BuildException("Timeout in tail.");
			else
				output.append("Timeout, terminating tail.\n");
		}
		else
			throw new BuildException("Unknown reason for terminating tail.");
	}
	
	
	




	public void init(Tailer tailer) {
		this.tailer = tailer;
	}

	public void fileNotFound() {		
		error = new FileNotFoundException("The file "+file+" could not be found.");
		error.printStackTrace(output);
		tailer.stop();
	}

	public void fileRotated() {
		output.append(this.getClass().getName()+": file rotation, now tailing file: "+tailer.getFile());
		output.append("\n");
	}

	public void handle(String line) {
		output.append(line);
		output.append("\n");
		if (pattern!=null && pattern.matcher(line).find()){
			if (regExFound < 0)
				regExFound = lineNo;
			tailer.stop();
		}
		lineNo++;
	}

	public void handle(Exception ex) {
		ex.printStackTrace(output);
		error = ex;
		tailer.stop();
	}




	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public Long getTimeout() {
		return timeout;
	}
	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}







	public boolean isFailOnTimeout() {
		return failOnTimeout;
	}







	public void setFailOnTimeout(boolean failOnTimeout) {
		this.failOnTimeout = failOnTimeout;
	}







	public boolean isFailOnRegex() {
		return failOnRegex;
	}







	public void setFailOnRegex(boolean failOnRegex) {
		this.failOnRegex = failOnRegex;
	}







	public boolean isFailOnMissingFile() {
		return failOnMissingFile;
	}







	public void setFailOnMissingFile(boolean failOnMissingFile) {
		this.failOnMissingFile = failOnMissingFile;
	}
	
}
